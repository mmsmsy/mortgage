FROM node AS build

WORKDIR /app
COPY . .

RUN npm ci
RUN npm run build

FROM nginx:stable

COPY --from=build /app/build/ /var/www/
COPY nginx.conf /etc/nginx/nginx.conf
EXPOSE 80

CMD ["nginx-debug", "-g", "daemon off;"]