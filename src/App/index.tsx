import React, { ReactElement } from 'react'
import styled from 'styled-components'

import {palette} from 'AppSource/ui'

import {Navigation} from './Navigation'
import {Calculators} from './Calculators'
import {Route, Switch, RouteComponentProps} from 'react-router-dom'
import { Root } from './Root'
import { NotFound } from 'AppSource/utilities/NotFound'

const ContentHolder = styled.div`
  display: flex;
  flex-direction: column;

  padding: 0 44px 10px 44px;
`

const Holder = styled.div`
  display: flex;
  flex-direction: column;

  min-height: 100vh;
  max-width: 100vw;

  background-color: ${palette.chartMidnightGreen.RGB};
  background: ${`linear-gradient(
    110deg,
    ${palette.chartMidnightGreen.RGB} 60%,
    ${palette.chartPurpleNavy.RGB} 70%,
    ${palette.chartPurpleNavy.RGB} 80%,
    ${palette.chartPastelRed.RGB} 80%,
    ${palette.chartPastelRed.RGB} 90%,
    ${palette.chartCheese.RGB} 95%
  )`};
`

export type AllowedRoute =
  | {name: 'calculators', component: React.ComponentType<RouteComponentProps<any>> | React.ComponentType<any>}
  | {name: 'mortgage', component: React.ComponentType<RouteComponentProps<any>> | React.ComponentType<any>}
  | {name: 'car financing', component: React.ComponentType<RouteComponentProps<any>> | React.ComponentType<any>}

const routes: AllowedRoute[] = [
  {name: 'calculators', component: Calculators}
]

export function App() {
  return (
    <Holder>
      <Navigation routes={routes}/>
      <ContentHolder>
        <Switch>
          <Route exact path='/' component={Root}/>
          {routes.map(route => (
            <Route key={route.name} path={`/`} component={route.component}/>
          ))}
          <Route component={NotFound}/>
        </Switch>
      </ContentHolder>
    </Holder>
  )
}