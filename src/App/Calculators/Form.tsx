import React from 'react'
import styled from 'styled-components'

import {containerBorderStyle} from 'AppSource/ui/border'
import {NumberInput} from 'AppSource/ui/inputs/NumberInput'
import {ToggleInput} from 'AppSource/ui/inputs/ToggleInput'
import {opacity} from 'AppSource/ui/opacity'
import {palette} from 'AppSource/ui/palette'

const Holder = styled.div`
  display: flex;
  flex-wrap: wrap;

  border-right: ${() => containerBorderStyle};
  border-left: ${() => containerBorderStyle};
  padding: 10px 3%;

  background-color: ${`rgba(${palette.white.RGBValues}, ${opacity.semiTransparent})`};

  * > {
    flex: 1;
    margin-bottom: 10px;
  }
`

const Separator = styled.span`
  border: ${() => containerBorderStyle};
  height: 24px;

  background-color: ${`rgba(${palette.white.RGBValues}, ${opacity.transparent})`};
`

interface Field {
  label: string,
  setter: (number: number | object | boolean) => void,
  unit?: string,
  value: number | object | boolean
}

interface Props {
  fields: Field[]
}

export function Form({fields}: Props) {
  return (
    <>
      <Holder>
        {fields.map(field => (
          typeof field.value === 'boolean'
            ? (
              <ToggleInput
                key={field.label}
                isOn={field.value}
                label={field.label}
                onChange={() => {field.setter(!field.value)}}
              />
            )
            : (
              typeof field.value === 'number'
                ? (
                  <NumberInput
                    key={field.label}
                    label={`${field.label} ${field.unit || ''}`}
                    min={0}
                    onChange={e => {
                      field.setter(e.currentTarget.valueAsNumber >= 0 ? e.currentTarget.valueAsNumber : 0)
                    }}
                    step={field.unit === '%' ? '0.01' : '1'}
                    value={field.value}
                  />
                )
                : (
                  Object.keys(field.value).map(valueKey => (
                    <NumberInput
                      key={field.label + valueKey}
                      label={`${field.label} ${valueKey.replace(/([A-Z])/g, ' $1')} ${field.unit ? `(${field.unit})` : ''}`}
                      min={valueKey === 'monthNumber' ? 1 : 0}
                      onChange={e => {
                        field.setter({
                          ...(typeof field.value === 'object' ? field.value : {}),
                          [valueKey]: valueKey === 'monthNumber'
                            ? e.currentTarget.valueAsNumber >= 1 ? e.currentTarget.valueAsNumber : 1
                            : e.currentTarget.valueAsNumber >= 0 ? e.currentTarget.valueAsNumber : 0
                        })
                      }}
                      step={field.unit === '%' ? '0.01' : '1'}
                      value={field.value[valueKey]}
                    />
                  ))
                )
            )
        ))}
      </Holder>
      
      <Separator/>
    </>
  )
}