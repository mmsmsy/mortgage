import React, { useState } from 'react'
import styled from 'styled-components'

import {Form} from '../Form'
import {Results} from './Results'

const Holder = styled.div`
  display: flex;
  flex-direction: column;
`

export function CarFinancing() {
  const [carValue, setCarValue] = useState<number>(100000)
  const [termMonths, setTermMonths] = useState<number>(60)
  const [residualValue, setResidualValue] = useState<number>(40000)

  return (
    <Holder>
      <Form fields={[
        {label: 'car value', setter: setCarValue, value: carValue},
        {label: 'term months', setter: setTermMonths, value: termMonths},
        {label: 'residual value', setter: setResidualValue, value: residualValue}
      ]}/>
      <Results
        carValue={carValue}
        termMonths={termMonths}
        residualValue={residualValue}
      />
    </Holder>
  )
}