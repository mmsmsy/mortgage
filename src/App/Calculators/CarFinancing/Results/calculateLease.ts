interface LeaseProps {
  interestRate: number,
  residualValue: number,
  termMonths: number,
  value: number
}

export function calculateLease({interestRate, residualValue, termMonths, value}: LeaseProps) {
  const monthlyInterestRate = interestRate / 1200
  const monthlyPayment =
    (
      value
      - (
        residualValue
        / Math.pow(1 + monthlyInterestRate, termMonths)
      )
    )
    / (
      ( 1 - ( 1 / Math.pow(1 + monthlyInterestRate, termMonths) ) )
      / monthlyInterestRate
    )

  const totalAmount = monthlyPayment * termMonths + residualValue

  return {monthlyPayment, totalAmount}
}