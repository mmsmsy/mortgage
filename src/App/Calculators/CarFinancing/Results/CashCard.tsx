import React from 'react'
import styled from 'styled-components'

import {borderRadius} from 'AppSource/ui/border'
import {palette} from 'AppSource/ui/palette'
import {numberFormatter} from 'AppSource/utilities/formatter'

const ItemHolder = styled.div`
  display: flex;
`

const Holder = styled.div`
  display: flex;
  flex-direction: column;

  border: 1px solid ${palette.silver.RGB};
  border-radius: ${borderRadius.medium};
  padding: 44px;

  background-color: ${palette.chartMidnightGreen.RGB};
  color: ${palette.white.RGB};
`

interface Props {
  carValue: number,
  termMonths: number,
  residualValue: number
}

export function CashCard({carValue, termMonths, residualValue}: Props) {
  return (
    <Holder>
      <ItemHolder style={{marginTop: 'auto'}}>
        <span>Monthly cost of ownership for equivalent time: {numberFormatter.format(carValue / termMonths)}</span>
      </ItemHolder>
      <ItemHolder>
        <span>Total cost of ownership for equivalent time: {numberFormatter.format(carValue - residualValue)}</span>
      </ItemHolder>
    </Holder>
  )
}