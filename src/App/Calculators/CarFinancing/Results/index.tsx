import React, { useState } from 'react'
import styled from 'styled-components'

import {palette} from 'AppSource/ui'
import { numberFormatter } from 'AppSource/utilities/formatter'
import { CashCard } from './CashCard'
import { LeaseCard } from './LeaseCard'
import { opacity } from 'AppSource/ui'
import { containerBorderStyle } from 'AppSource/ui'

const Info = styled.div`
  font-size: 24px;
  font-weight: bold;
  line-height: 36px;
`

const InfoRow = styled.div`
  display: flex;

  > * {
    flex: 1;
  }
`

const InfoHolder = styled.div`
  display: flex;
  flex-direction: column;

  border-bottom: 1px solid ${palette.black.RGB};

  background-color: ${palette.white.RGB};

  :last-child {
    border-bottom: none;
  }
`

const Holder = styled.div`
  display: flex;
  flex-direction: column;

  border: ${() => containerBorderStyle};
  border-top: none;
  padding: 44px 3%;
  
  background-color: ${`rgba(${palette.white.RGBValues}, ${opacity.semiTransparent})`};
`

interface Props {
  carValue: number,
  termMonths: number,
  residualValue: number
}

export function Results({carValue, termMonths, residualValue}: Props) {
  const [leaseTotalAmount, setLeaseTotalAmount] = useState<number>(0)

  return (
    <Holder>
      <InfoHolder>
        <InfoRow>
          <Info>Item</Info>
          <Info><a href="https://www.youtube.com/watch?v=6ZirY4lEOGM" target="_blank">Cash money!</a></Info>
          <Info>Lease</Info>
        </InfoRow>
        <InfoRow>
          <Info>Assumptions</Info>
          <CashCard carValue={carValue} termMonths={termMonths} residualValue={residualValue}/>
          <LeaseCard
            carValue={carValue}
            termMonths={termMonths}
            residualCarValue={residualValue}

            setLeaseTotalAmount={setLeaseTotalAmount}
          />
        </InfoRow>
        <InfoRow>
          <Info>Total cost</Info>
          <Info>{numberFormatter.format(carValue)}</Info>
          <Info>{numberFormatter.format(leaseTotalAmount + residualValue)}</Info>
        </InfoRow>
      </InfoHolder>
    </Holder>
  )
}