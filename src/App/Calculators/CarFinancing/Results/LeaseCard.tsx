import React, { useState } from 'react'
import styled from 'styled-components'
import { borderRadius } from 'AppSource/ui'
import { palette } from 'AppSource/ui'
import { NumberInput } from 'AppSource/ui/inputs/NumberInput'
import { numberFormatter } from 'AppSource/utilities/formatter'
import { calculateLease } from './calculateLease'

const ItemHolder = styled.div`
  display: flex;
`

const Holder = styled.div`
  display: flex;
  flex-direction: column;

  border: 1px solid ${palette.silver.RGB};
  border-radius: ${borderRadius.medium};
  padding: 44px;

  background-color: ${palette.chartMidnightGreen.RGB};
  color: ${palette.white.RGB};
`

interface Props {
  carValue: number,
  termMonths: number,
  residualCarValue: number,
  
  setLeaseTotalAmount: (number: number) => void
}

export function LeaseCard({carValue, termMonths, residualCarValue, setLeaseTotalAmount}: Props) {
  const [downPayment, setDownPayment] = useState<number>(10000)
  const [interestRate, setInterestRate] = useState<number>(2)

  const {monthlyPayment, totalAmount} = calculateLease({interestRate, residualValue: residualCarValue, termMonths, value: carValue - downPayment})
  setLeaseTotalAmount(monthlyPayment * termMonths + downPayment)

  return (
    <Holder>
      <ItemHolder>
        <NumberInput
          label='down payment'
          onChange={e => {setDownPayment(e.currentTarget.valueAsNumber >= 0 ? e.currentTarget.valueAsNumber : 0)}}
          min={0}
          step='1'
          value={downPayment}
        />
      </ItemHolder>
      <ItemHolder>
        <NumberInput
          label='interest rate - yearly (%)'
          onChange={e => {setInterestRate(e.currentTarget.valueAsNumber >= 0 ? e.currentTarget.valueAsNumber : 0)}}
          min={0}
          step='0.01'
          value={interestRate}
        />
      </ItemHolder>
      <ItemHolder>
        <span>Lease payment: {numberFormatter.format(monthlyPayment)}</span>
      </ItemHolder>
      <ItemHolder>
        <span>Total interests: {numberFormatter.format(totalAmount + downPayment - carValue)}</span>
      </ItemHolder>
      <ItemHolder style={{marginTop: 'auto'}}>
        <span>Total cost of ownership for the lease term: {numberFormatter.format((totalAmount + downPayment - residualCarValue))}</span>
      </ItemHolder>
    </Holder>
  )
}