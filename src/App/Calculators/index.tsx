import React from 'react'
import styled from 'styled-components'

import {Mortgage} from './Mortgage'
import {blur, containerBorderStyle, opacity, palette} from 'AppSource/ui'
import { NavLink, RouteComponentProps, Route, Switch, Redirect } from 'react-router-dom'
import { AllowedRoute } from '..'
import { NotFound } from 'AppSource/utilities/NotFound'
import { CarFinancing } from './CarFinancing'

const Tab = styled(NavLink)`
  border: ${() => containerBorderStyle};
  border-bottom: 0;
  height: 44px;
  opacity: ${opacity.semiTransparent};
  padding: 0 10px;
  transition: all 0.15s;

  background-color: ${palette.chartMidnightGreen.RGB};
  color: ${palette.white.RGB};
  font-size: 24px;
  font-weight: bold;
  line-height: 44px;
  text-decoration: none;

  :first-letter {
    text-transform: uppercase;
  }
  &.tab-active,
  :active,
  :hover {
    opacity: 1;

    background-color: ${`rgba(${palette.white.RGBValues}, ${opacity.transparent})`};
  }
`

const Tabs = styled.div`
  display: flex;
`

const TitleBottomSeparator = styled.span`
  display: flex;

  border: ${() => containerBorderStyle};
  border-top: 0;
  height: 24px;
  width: 100%;

  backdrop-filter: ${`blur(${blur.default})`};
  background-color: ${`rgba(${palette.white.RGBValues}, ${opacity.transparent})`};
`

const Holder = styled.div`
  display: flex;
  flex-direction: column;
`

const calculators: AllowedRoute[] = [
  {name: 'mortgage', component: Mortgage},
  {name: 'car financing', component: CarFinancing}
]

interface Props extends RouteComponentProps {}

export function Calculators({match}: Props) {
  return (
    <Holder>
      <Tabs>
        {calculators.map(calculator => (
          <Tab
            key={calculator.name}
            activeClassName='tab-active'
            to={`${match.path}/${calculator.name}`}
          >{calculator.name}</Tab>
        ))}
      </Tabs>
      <TitleBottomSeparator/>
      <Switch>
        <Redirect exact from={`${match.path}/`} to={`${match.path}/${calculators[0].name}`}/>
        {calculators.map(calculator => (
          <Route
            key={calculator.name}
            path={`${match.path}/${calculator.name}`}
            component={calculator.component}
          />
        ))}
        <Route component={NotFound}/>
      </Switch>
    </Holder>
  )
}