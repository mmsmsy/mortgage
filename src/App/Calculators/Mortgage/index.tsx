import React, {useState} from 'react'
import styled from 'styled-components'

import {blur} from 'AppSource/ui'

import {Form} from '../Form'
import { Results } from './Results'

const Holder = styled.div`
  display: flex;
  flex-direction: column;

  backdrop-filter: ${`blur(${blur.default})`};
`

export interface LumpSumOverpayment {
  amount: number,
  monthNumber: number
}

export function Mortgage() {
  const [debt, setDebt] = useState<number>(200000)
  const [termYears, setTermYears] = useState<number>(15)
  const [termMonths, setTermMonths] = useState<number>(0)
  const [interestRate, setInterestRate] = useState<number>(4.18)
  const [lumpSumOverpayment, setLumpSumOverpayment] = useState<LumpSumOverpayment>({
    amount: 0,
    monthNumber: 1
  })
  const [monthlyOverpayment, setMonthlyOverpayment] = useState<number>(0)
  const [useFreedMoney, setUseFreedMoney] = useState<boolean>(false)
  const [shouldAdjustMonthlyPayment, setShouldAdjustMonthlyPayment] = useState<boolean>(false)

  return (
    <Holder>
      <Form fields={[
        {label: 'debt', setter: setDebt, value: debt},
        {label: 'term years', setter: setTermYears, value: termYears},
        {label: 'term months', setter: setTermMonths, value: termMonths},
        {label: 'interest rate', setter: setInterestRate, unit: '%', value: interestRate},
        {label: 'lump sum', setter: setLumpSumOverpayment, value: lumpSumOverpayment},
        {label: 'monthly', setter: setMonthlyOverpayment, value: monthlyOverpayment},
        {label: 'adjust payment', setter: setShouldAdjustMonthlyPayment, value: shouldAdjustMonthlyPayment},
        {label: 'use freed money', setter: setUseFreedMoney, value: useFreedMoney}
      ]}/>
      <Results
        debt={debt}
        interestRate={interestRate}
        lumpSumOverpayment={{
          amount: lumpSumOverpayment.amount,
          monthNumber: lumpSumOverpayment.monthNumber
        }}
        monthlyOverpayment={monthlyOverpayment}
        termMonths={termMonths}
        termYears={termYears}
        useFreedMoney={useFreedMoney}
        shouldAdjustMonthlyPayment={shouldAdjustMonthlyPayment}
      />
    </Holder>
  )
}