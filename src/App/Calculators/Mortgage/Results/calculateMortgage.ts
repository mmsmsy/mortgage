import {LumpSumOverpayment} from ".."

function calculateMonthlyPayment(
  monthlyInterestRate: number,
  remainingPrincipal: number,
  remainingPeriods: number
): number {
  return (
    monthlyInterestRate * remainingPrincipal
    / ( 1 - Math.pow( (1 + monthlyInterestRate), -remainingPeriods ) )
  )
}

interface CalculateMortgageProps {
  useFreedMoney?: boolean,
  debt: number,
  interestRate: number,
  lumpSumOverpayment?: LumpSumOverpayment,
  monthlyOverpayment?: number,
  shouldAdjustMonthlyPayment?: boolean,
  termMonths: number,
  termYears: number
}

export const calculateMortgage = ({
  useFreedMoney,
  debt,
  interestRate,
  lumpSumOverpayment,
  monthlyOverpayment,
  shouldAdjustMonthlyPayment,
  termMonths,
  termYears
}: CalculateMortgageProps
): Array<number[]> => {
  const monthlyInterestRate = interestRate / 1200
  const termPeriods = termYears * 12 + termMonths
  let remainingPrincipal = debt
  let interestsPaid = 0
  const monthlyPaymentRates = [{
    amount: calculateMonthlyPayment(monthlyInterestRate, debt, termPeriods),
    firstMonth: 0
  }]

  const chartData = [[
    0,                  // interests paid to date
    remainingPrincipal, // remaining principal
    0,                  // default payment
    0,                  // paid this month
  ]]

  for (let i = 1; i <= termPeriods; i++) {
    const interestsThisMonth = monthlyInterestRate * remainingPrincipal
    interestsPaid += interestsThisMonth

    const paymentThisMonth = monthlyPaymentRates[monthlyPaymentRates.length - 1].amount

    const overpaymentThisMonth =
      (lumpSumOverpayment?.monthNumber === i ? lumpSumOverpayment.amount : 0)
      + (monthlyOverpayment ? monthlyOverpayment : 0)
      + (useFreedMoney ? (monthlyPaymentRates[0].amount - paymentThisMonth) : 0)


    const principalThisMonth = paymentThisMonth - interestsThisMonth + overpaymentThisMonth

    remainingPrincipal = remainingPrincipal - principalThisMonth < 0
      ? 0
      : remainingPrincipal - principalThisMonth

    if (shouldAdjustMonthlyPayment && overpaymentThisMonth > 0) {
      monthlyPaymentRates.push({
        amount: calculateMonthlyPayment(monthlyInterestRate, remainingPrincipal, termPeriods - i),
        firstMonth: i + 1
      })
    }

    chartData.push([
      interestsPaid,                          // interests paid to date
      remainingPrincipal,                     // remaining principal
      paymentThisMonth,                       // default payment
      interestsThisMonth + principalThisMonth // paid this month
    ])
  }

  return chartData
}