import React, { useState } from 'react'
import styled from 'styled-components'
import { numberFormatter } from 'AppSource/utilities/formatter'
import { palette } from 'AppSource/ui/palette'

const Tooltip = styled.div`
  display: flex;
  flex-direction: column;
  
  padding: 10px;
  position: absolute;
    top: 0;
    left: 0;

  background-color: white;
`

const Chart = styled.svg`
  height: 400px;
  width: 100%;

  stroke: #000;
  stroke-width: 0;
  stroke-linecap: round;
  stroke-linejoin: round;

  g {
    > :nth-child(1) {
      fill: ${palette.chartCheese.RGB};
    }
    > :nth-child(2) {
      fill: ${palette.chartPastelRed.RGB};
    }
    > :nth-child(3) {
      fill: ${palette.chartPurpleNavy.RGB};
    }
    > :nth-child(4) {
      fill: ${palette.chartMidnightGreen.RGB};
    }
  }
`

const Holder = styled.div`
  display: flex;

  padding: 48px 0;
  position: relative;
  width: 90%;
`

interface Props {
  data: Array<number | number[]>,
  dataWithOverpayment: Array<number[]>,
  principal: number
}

export function NewChart({data, dataWithOverpayment, principal}: Props) {
  interface TooltipInfo {
    interestsPaid: string,
    payment: string
  }

  const [tooltipInfo, setTooltipInfo] = useState<TooltipInfo | null>(null)

  const barWidth = 100 / data.length
  const totalDebt = principal + data[data.length - 1][0]

  const totalInterests = data[data.length - 1][0]
  const totalInterestsHeight = totalInterests / totalDebt * 100

  return (
    <Holder onMouseOut={() => {setTooltipInfo(null)}}>
      <Chart>
          {data.map((item, index) => {
            const x = `${barWidth*index}%`
            const interestsPaid = item[0] / totalDebt * 100
            const interestsToPay = (totalInterests - item[0]) / totalDebt * 100

            const principalToPay = item[1] / totalDebt * 100
            const principalPaid = totalDebt - item[1] / totalDebt * 100

            return (
              <g key={index} onMouseOver={() => {setTooltipInfo({
                interestsPaid: numberFormatter.format(item[0]),
                payment: numberFormatter.format(item[3]),

              })}}>
                <rect x={x} y="0" width={`${barWidth}%`} height={`${interestsPaid}%`}></rect>
                <rect x={x} y={`${interestsPaid}%`} width={`${barWidth}%`} height={`${interestsToPay}%`}></rect>
                <rect x={x} y={`${totalInterestsHeight}%`} width={`${barWidth}%`} height={`${principalToPay}%`}></rect>
                <rect x={x} y={`${totalInterestsHeight+principalToPay}%`} width={`${barWidth}%`} height={`${principalPaid}%`}></rect>
              </g>
            )
          })}
      </Chart>
      {tooltipInfo && (
        <Tooltip>
          <span>Interests paid: {tooltipInfo.interestsPaid}</span>
          <span>Payment: {tooltipInfo.payment}</span>
        </Tooltip>
      )}
    </Holder>
  )
}