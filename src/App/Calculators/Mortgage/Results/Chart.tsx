import React from 'react'
import {ResponsiveContainer, BarChart, CartesianGrid, Tooltip, XAxis, YAxis, Bar} from 'recharts'

import {numberFormatter} from 'AppSource/utilities/formatter'
import {palette} from 'AppSource/ui'

// interface Payload {
//   value: string
// }

// interface TooltipProps {
//   active: boolean,
//   payload: Payload[],
//   label: string
// }

// function CustomTooltip({active, payload, label}: TooltipProps) {
//   if (active) {
//     return (
//       <div className="custom-tooltip">
//         <p className="label">{`${label} : ${payload[0].value}`}</p>
//         <p className="intro">{label}</p>
//         <p className="desc">Anything you want can be displayed here.</p>
//       </div>
//     );
//   }

//   return null
// }

interface ChartProps {
  isOverpaymentInfoVisible: boolean,
  data: Array<number[]>
}

export function Chart({isOverpaymentInfoVisible, data}: ChartProps) {
  return (
    <ResponsiveContainer width='100%' height={500}>
      <BarChart
        data={data}
        barGap={0}
        barCategoryGap={0}
        margin={{top: 22, right: 64, bottom: 24, left: 64}}
      >
        <CartesianGrid/>
        <Tooltip
          formatter={value => numberFormatter.format(value as unknown as number)}
          isAnimationActive={false}
        />
        <XAxis dataKey="name"/>
        <YAxis tickFormatter={numberFormatter.format}/>
        <Bar dataKey="remaining principal" stackId='a' fill={palette.chartMidnightGreen.RGB}/>
        <Bar hide={!isOverpaymentInfoVisible} dataKey="remaining principal with overpayment" stackId='b' fill={palette.chartMidnightGreen.RGB}/>
        <Bar dataKey="principal paid to date" stackId='a' fill={palette.chartPurpleNavy.RGB}/>
        <Bar hide={!isOverpaymentInfoVisible} dataKey="principal paid to date with overpayment" stackId='b' fill={palette.chartPurpleNavy.RGB}/>
        <Bar dataKey="remaining interests" stackId='a' fill={palette.chartPastelRed.RGB}/>
        <Bar hide={!isOverpaymentInfoVisible} dataKey="remaining interests with overpayment" stackId='b' fill={palette.chartPastelRed.RGB}/>
        <Bar dataKey="interests paid to date" stackId='a' fill={palette.chartCheese.RGB}/>
        <Bar hide={!isOverpaymentInfoVisible} dataKey="interests paid to date with overpayment" stackId='b' fill={palette.chartCheese.RGB}/>
        <Bar dataKey="default payment" stackId='a' fill={palette.chartMulberry.RGB} maxBarSize={0}/>
        <Bar hide={!isOverpaymentInfoVisible} dataKey="default payment with overpayment" stackId='b' fill={palette.chartMulberry.RGB} maxBarSize={0}/>
        <Bar dataKey="paid this month" stackId='a' fill={palette.black.RGB} maxBarSize={0}/>
        <Bar hide={!isOverpaymentInfoVisible} dataKey="paid this month with overpayment" stackId='b' fill={palette.black.RGB} maxBarSize={0}/>
      </BarChart>
    </ResponsiveContainer>
  )
}