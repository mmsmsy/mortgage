import React from 'react'
import styled from 'styled-components'

import {palette} from 'AppSource/ui/palette'
import {numberFormatter} from 'AppSource/utilities/formatter'

import { opacity } from 'AppSource/ui/opacity'
import { borderRadius } from 'AppSource/ui/border'

const Description = styled.span``

const Principal = styled.span`
  color: ${palette.chartMidnightGreen.RGB};
`

const Interests = styled.span`
  color: ${palette.chartPastelRed.RGB};
`

const Sum = styled.span``

interface InfoProps {
  isHead?: boolean
}

function getInfoSpanFontSize(isHead: InfoProps['isHead']) {
  if (!isHead) return '24px'
  return '18px'
}

const Info = styled.div`
  display: flex;
  justify-content: space-around;
  align-items: center;

  border-bottom: ${`1px solid rgba(${palette.black.RGBValues}, ${opacity.transparent})`};
  overflow: hidden;

  :last-child {
    border-bottom: none;
  }

  > *:last-child {
    border-right: none;
  }

  span {
    flex: 1;

    border-right: ${`1px solid rgba(${palette.black.RGBValues}, ${opacity.transparent})`};

    font-size: ${(props: InfoProps) => getInfoSpanFontSize(props.isHead)};
    font-weight: bold;
    line-height: 36px;
    text-align: center;
  }
`

const Holder = styled.div`
  display: flex;
  flex-direction: column;

  border-radius: ${borderRadius.medium} ${borderRadius.medium} 0 0;
  padding-bottom: 30px;

  background-color: ${palette.white.RGB};
`

interface InfoCompProps {
  interests: number,
  interestsWithOverpayment: number,
  isOverpaymentInfoVisible: boolean,
  principal: number
}

export function InfoComp({
  interests,
  interestsWithOverpayment,
  isOverpaymentInfoVisible,
  principal
}: InfoCompProps) {
  return (
    <Holder>
      <Info isHead>
        <Description>Case</Description>
        <Principal>Principal</Principal>
        <Interests>Interests</Interests>
        <Sum>Mortgage sum</Sum>
      </Info>
      <Info>
        <Description>Default</Description>
        <Principal>{numberFormatter.format(principal)}</Principal>
        <Interests>{numberFormatter.format(interests)}</Interests>
        <Sum>{numberFormatter.format(principal + interests)}</Sum>
      </Info>
      {isOverpaymentInfoVisible && (
        <Info>
          <Description>With overpayment</Description>
          <Principal>-|-</Principal>
          <Interests>{numberFormatter.format(interestsWithOverpayment)}</Interests>
          <Sum>{numberFormatter.format(principal + interestsWithOverpayment)}</Sum>
        </Info>
      )}
    </Holder>
  )
}