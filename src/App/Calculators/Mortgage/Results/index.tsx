import React, {useState} from 'react'
import styled from 'styled-components'

import {opacity} from 'AppSource/ui/opacity'
import {palette} from 'AppSource/ui/palette'

import {LumpSumOverpayment} from '..'
import {calculateMortgage} from './calculateMortgage'
// import {Chart} from './Chart'
import { InfoComp } from './Info'
import { containerBorderStyle } from 'AppSource/ui/border'
import { Toggle } from 'AppSource/ui/inputs/toggle'
import { NewChart } from './NewChart'

const InputLabel = styled.span`
  margin-right: 4px;

  color: ${palette.white.RGB};
  font-size: 12px;
  font-weight: 500;
  line-height: 20px;
`

const InputHolder = styled.div`
  display: flex;
  align-items: center;

  position: absolute;
    bottom: 10px;
    right: 86px;
`

const ChartHolder = styled.div`
  display: flex;
  justify-content: center;

  background-color: ${palette.white.RGB};
`

const Holder = styled.div`
  display: flex;
  flex-direction: column;

  border: ${() => containerBorderStyle};
  border-top: none;
  padding: 44px 3%;
  position: relative;

  background-color: ${`rgba(${palette.white.RGBValues}, ${opacity.semiTransparent})`};
`

interface Props {
  useFreedMoney: boolean,
  debt: number,
  interestRate: number,
  lumpSumOverpayment: LumpSumOverpayment,
  monthlyOverpayment: number,
  shouldAdjustMonthlyPayment: boolean,
  termMonths: number,
  termYears: number
}

export function Results({
  useFreedMoney,
  debt,
  interestRate,
  lumpSumOverpayment,
  monthlyOverpayment,
  shouldAdjustMonthlyPayment,
  termMonths,
  termYears
}: Props) {
  const [isSimplified, setIsSimplified] = useState<boolean>(false)

  const isOverpaymentInfoVisible = (lumpSumOverpayment.amount || monthlyOverpayment) ? true : false

  const data = calculateMortgage({debt, interestRate, termMonths, termYears})

  const dataWithOverpayment
    = calculateMortgage({
      useFreedMoney,
      debt,
      interestRate,
      lumpSumOverpayment,
      monthlyOverpayment,
      shouldAdjustMonthlyPayment,
      termMonths,
      termYears
    })

  return (
    <Holder>
      <InputHolder>
        <InputLabel>Show every 10th month</InputLabel>
        <Toggle isOn={isSimplified} onChange={() => {setIsSimplified(!isSimplified)}}/>
      </InputHolder>
      <InfoComp
        interests={data[data.length - 1][0]}
        interestsWithOverpayment={dataWithOverpayment[data.length - 1][0]}
        isOverpaymentInfoVisible={isOverpaymentInfoVisible}
        principal={debt}
      />

      <ChartHolder>
        <NewChart
          data={isSimplified ? data.filter((item, index) => {
            if (index === 0 || index % 10 === 0 || index === data.length) return true
            else return false
          }) : data}
          dataWithOverpayment={dataWithOverpayment}
          principal={debt}
        />
      </ChartHolder>

    </Holder>
  )
}