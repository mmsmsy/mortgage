import React from 'react'
import styled from 'styled-components'
import {NavLink, Link} from 'react-router-dom'

import {opacity, palette} from 'AppSource/ui'
import {AllowedRoute} from '.'

const TitleLink = styled(Link)`
  height: 44px;
  padding: 0 44px;

  background-color: ${`rgba(${palette.white.RGBValues}, ${opacity.transparent})`};
  color: ${`rgba(${palette.white.RGBValues}, ${opacity.semiTransparent})`};
  font-size: 24px;
  font-weight: bold;
  line-height: 44px;
  text-decoration: none;
  text-transform: uppercase;
`

const CustomNavLink = styled(NavLink)`
  height: 44px;
  padding: 0 10px;

  color: ${palette.white.RGB};
  font-size: 18px;
  line-height: 44px;
  text-decoration: none;
  text-transform: capitalize;
  transition: all 0.15s;

  :active,
  :hover {
    background-color: ${`rgba(${palette.white.RGBValues}, ${opacity.opaque})`};
    color: ${palette.black.RGB};
  }

  &.nav-link-active {
    background-color: ${`rgba(${palette.white.RGBValues}, ${opacity.opaque})`};
    color: ${palette.black.RGB};
  }
`

const NavLinks = styled.div`
  display: flex;

  padding-left: 64px;

  background-color: ${`rgba(${palette.white.RGBValues}, ${opacity.semiTransparent})`};
`

const RemainingNavHolder = styled.span`
  flex: 1;

  height: 44px;
  
  background-color: ${`rgba(${palette.white.RGBValues}, ${opacity.semiTransparent})`};
`

const Holder = styled.div`
  display: flex;

  margin-bottom: 44px;
  width: 100%;
`

interface Props {
  routes: AllowedRoute[]
}

export function Navigation({routes}: Props) {
  return (
    <Holder>
      <TitleLink to='/'>mmsmsy.tech</TitleLink>
      <NavLinks>
        {routes.map(route => (
          <CustomNavLink
            key={route.name}
            activeClassName='nav-link-active'
            to={`/${route.name}`}
          >{route.name}</CustomNavLink>
        ))}
      </NavLinks>
      <RemainingNavHolder/>
    </Holder>
  )
}