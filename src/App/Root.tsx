import React from 'react'
import styled from 'styled-components'
import { palette } from 'AppSource/ui/palette'

const WelcomeMessage = styled.span`
  color: ${palette.white.RGB};
`

const Holder = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  height: 500px;
  width: 100%;
`

export function Root() {
  return (
    <Holder>
      <WelcomeMessage>Welcome to MMSMSY</WelcomeMessage>
    </Holder>
  )
}