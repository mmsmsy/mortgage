const generateColor = (RGBValues: string) => ({RGB: `rgb(${RGBValues})`, RGBValues})

export const palette = {
  black: generateColor('10,10,10'),
  grey: generateColor('223,223,223'),
  silver: generateColor('204,204,204'),
  white: generateColor('225,225,225'),

  statusGreen: generateColor('96,169,166'),

  chartMidnightGreen: generateColor('0,63,92'),
  chartPurpleNavy: generateColor('88,80,141'),
  chartMulberry: generateColor('188,80,144'),
  chartPastelRed: generateColor('255,99,97'),
  chartCheese: generateColor('255,166,0')
}

export const blur = {
  default: '10px'
}

export const opacity = {
  transparent: 0.25,
  semiTransparent: 0.5,
  opaque: 0.75
}

export const borderRadius = {
  small: '8px',
  medium: '16px',
  large: '24px'
}

export const containerBorderStyle = `1px solid rgba(${palette.white.RGBValues}, ${opacity.transparent})`
