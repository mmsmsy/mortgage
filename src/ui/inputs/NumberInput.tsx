import React from 'react'

import {Input, InputHolder, Label} from './inputs'

interface Props {
  label: string,
  min?: number | string,
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void,
  step?: number | string,
  value: number
}

export function NumberInput({label, onChange ,min, step, value}: Props) {
  return (
    <InputHolder>
      <Label>{label}</Label>
      <Input
        min={min}
        step={step}
        onChange={onChange}
        type='number'
        value={value.toString()}
      />
    </InputHolder>
  )
}