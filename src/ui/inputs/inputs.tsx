import styled from 'styled-components'
import {palette} from '../palette'
import {opacity} from '../opacity'

export const Label = styled.span`
color: ${palette.white.RGB};
font-size: 12px;
font-weight: bold;
line-height: 14px;
text-transform: capitalize;
`

export const Input = styled.input`
height: 30px;
padding: 0 4px;
min-width: 100%;
width: 150px;

border: none;

background-color: ${`rgba(${palette.white.RGBValues}, ${opacity.semiTransparent})`};
`

export const InputHolder = styled.div`
display: flex;
flex-direction: column;

margin-right: 10px;
margin-bottom: 10px;

${Label} {
  margin-right: 10px;
}
`