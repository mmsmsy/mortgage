import React from 'react'
import styled from 'styled-components'

import {palette} from '../palette'
import { opacity } from '../opacity'

interface ActuatorProps {
  isOn: boolean
}

function getActuatorMarginLeft(isOn: ActuatorProps['isOn']) {
  if (!isOn) return '0'
  return '50%'
}
const Actuator = styled.span`
  border: ${() => `1px solid rgba(${palette.silver.RGBValues}, ${opacity.semiTransparent})`};
  border-radius: 6px;
  height: 100%;
  margin-left: ${(props: ActuatorProps) => getActuatorMarginLeft(props.isOn)};
  transition: margin 0.25s;
  width: 50%;

  background-color: ${() => palette.white.RGB};
`

interface HolderProps {
  isOn: boolean
}

function getHolderBackgroundColor(isOn: HolderProps['isOn']) {
  if (!isOn) return palette.silver.RGB
  return palette.statusGreen.RGB
}

const Holder = styled.div`
  display: flex;

  border: ${() => `1px solid rgba(${palette.silver.RGBValues}, ${opacity.semiTransparent})`};
  border-radius: 8px;
  height: 30px;
  padding: 2px;
  transition: background-color 0.15s;
  width: 56px;

  background-color: ${(props: HolderProps) => getHolderBackgroundColor(props.isOn)};
  cursor: pointer;

  * {
    cursor: pointer;
  }
`

interface Props {
  isOn: boolean,
  onChange: () => void
}

export function Toggle({isOn, onChange}: Props) {
  return (
    <Holder isOn={isOn} onClick={onChange}>
      <Actuator isOn={isOn}/>
    </Holder>
  )
}