import React from 'react'

import {InputHolder, Label} from './inputs'
import {Toggle} from './toggle'

interface Props {
  label: string,
  isOn: boolean,
  onChange: () => void
}

export function ToggleInput({label, isOn, onChange}: Props) {
  return (
    <InputHolder>
      <Label>{label}</Label>
      <Toggle isOn={isOn} onChange={onChange}/>
    </InputHolder>
  )
}