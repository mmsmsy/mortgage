import React from 'react'
import ReactDOM from 'react-dom'
import {createGlobalStyle} from 'styled-components'
import {Normalize} from 'styled-normalize'

import {App}  from './App'
import { Router } from 'react-router-dom'
import { history } from './history'

const GlobalStyles = createGlobalStyle`
  html, body {
    box-sizing: border-box;
    
    font-family: 'Roboto', sans-serif;
    font-size: 18px;
    line-height: 28px;

    * {
      box-sizing: border-box;

      font-family: 'Roboto', sans-serif;
    }
  }
`

class Root extends React.Component<{}> {
  render() {
    return (
      <Router history={history}>
        <GlobalStyles/>
        <Normalize/>
        <App/>
      </Router>
    )
  }
}

ReactDOM.render(<Root/>, document.querySelector('#root'))

if (module.hot) {
  module.hot.accept()
}