import React from 'react'
import styled from 'styled-components'

import {palette} from 'AppSource/ui/palette'

const Message = styled.span`
  color: ${palette.white.RGB};
  font-size: 24px;
  font-weight: bold;
  line-height: 36px;
`

const Holder = styled.div`
  display: flex;
  
  height: 500px;
  width: 100%;
`

interface Props {}

export function NotFound({}: Props) {
  return (
    <Holder>
      <Message>Didn't find {window.location.pathname}</Message>
    </Holder>
  )
}