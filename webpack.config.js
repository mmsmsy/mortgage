const {CleanWebpackPlugin} = require('clean-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const Dotenv = require('dotenv-webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const path = require('path')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const webpack = require('webpack')

require('dotenv').config()

const babelOptions = {
  presets: [
    ["@babel/preset-env", {
      modules: 'commonjs',
      useBuiltIns: "usage",
      corejs: {
        version: 3,
        proposals: true
      }
    }],
    "@babel/preset-react"
  ]
}

module.exports = (env, argv) => ({
  devServer: {
    https: true,
    allowedHosts: [
      process.env.APP_HOST
    ],
    host: '0.0.0.0',
    port: process.env.PORT || 443,
    disableHostCheck: true,
    contentBase: path.join(__dirname, 'public'),
    historyApiFallback: true,
    publicPath: '/',
    hot: true,
    ...((process.env.APP_API_HOST && process.env.APP_API_HOST.indexOf('https') === -1) && {
      proxy: {
        [process.env.APP_API_HOST]: {
          target: process.env.APP_API_HOST,
          ignorePath: false,
          changeOrigin: true,
          secure: false
        }
      }
    })
  },
  devtool: 'inline-source-map',
  entry: './src/index.tsx',
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'bundle.[hash:22].min.js',
    chunkFilename: '[name].[chunkhash:22].js'
  },
  module: {
    rules: [
      {test: /\.tsx?$/, exclude: /node_modules/, use: [
        {loader: 'babel-loader', options: babelOptions},
        {loader: 'ts-loader'}
      ]},
      {test: /\.css$/, use: ['style-loader', 'css-loader']},
      {
        test: [/\.ttf$/, /\.png$/, /\.jpe?g$/, /\.svg$/],
        loader: 'url-loader',
        options: {
          limit: 10000,
          name: 'static/media/[name].[hash:8].[ext]'
        }
      }
    ]
  },
  optimization: {
    runtimeChunk: true,
    minimizer: [new UglifyJsPlugin({
      parallel: true,
      uglifyOptions: {
        output: {
          comments: false
        },
        compress: {
          drop_console: true
        }
      }
    })],
    splitChunks: {
      cacheGroups: {
        default: false,
        vendors: false,
        vendor: {
          name: 'vendor',
          chunks: 'all',
          test: /node_modules/
        }
      }
    },
    sideEffects: false
  },
  plugins: [
    new CleanWebpackPlugin(),
    new Dotenv({
      systemvars: process.env.NODE_ENV === 'production'
    }),
    new HtmlWebpackPlugin({
      title: 'mmsmsy.tech',
      template: './public/index.html'
    }),
    new CopyWebpackPlugin([
      {from: 'public/', to: '', ignore: ['index.html']}
    ], {}),
    new webpack.HotModuleReplacementPlugin()
  ],
  resolve: {
    extensions: ['.ts', '.tsx', '.js'],
    alias: {
      AppSource: path.resolve(__dirname, 'src/')
    }
  }
})